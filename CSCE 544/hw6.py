from importlib import import_module
import math
import glob
import random

# Iterative version of Extended Euclid Algorithm
def extEuclidIterative(a, b):

    # Assign values for s,t and previous values of s,t
    bezout_s, bezout_t = 0, 1
    prev_s, prev_t = 1, 0

    # While remainder is not 1
    while a != 0:

        # Perform the floor division and remainder op
        q = b // a
        r = b % a

        # Calculate new values of m and n
        m = (bezout_s - (prev_s * q)) 
        n = (bezout_t - (prev_t * q))

        # Assign the values for next iteration
        b, a = a, r
        bezout_s, bezout_t = prev_s, prev_t
        prev_s, prev_t = m, n

    # b is the gcd
    gcd = b

    return (gcd, bezout_s, bezout_t)

# Function to convert decimal to binary string
def int2bits(num):

    # Convert num to binary string (format out '0b')
    bits = "{0:b}".format(num)

    return bits

# Function to perform Square and Multiply Algorithm
def sqrMult(x, H, modulus):

    # Convert exponent to binary string
    H = int2bits(H)
    r = 1
    
    # For every num in H starting at index[1]
    for i in range(len(H)):
        # Square r
        r = (r * r) % modulus
        # If num at index i is '1'
        if(H[i] == '1'):
            # Multiply r by base 'x'
            r = (r * x) % modulus
            
    return r

# Function to perform exponentiation encryption
def RSAEncrypt(x, e, p, q):

    # Calculate n and do exponentiation
    n = p * q
    y = sqrMult(x, e, n)

    return y

# Function to perform RSA decryption using CRT
def CRTDecrypt(y, p, q, d):

    # Calculate n
    n = p * q

    # Calculate yp and yq
    yp = y % p
    yq = y % q

    # Calculate dp and dq
    dp = d % (p - 1)
    dq = d % (q - 1)

    # Calculate xp and xq
    xp = sqrMult(yp, dp, p)
    xq = sqrMult(yq, dq, q)

    # Calculate q and p inverse
    qInverse = extEuclidIterative(q, p)[1]
    pInverse = extEuclidIterative(p, q)[1]

    # Calculate cp and cq
    cp = qInverse % p
    cq = pInverse % q

    # Calculate decrypted text
    x = (((q * cp) * xp) + ((p * cq) * xq)) % n

    return x

# Function to perform Miller Rabin primality test
def millerRabin(num, s):
 
    # Set variables u and m for ease
    u = 0
    r =  num - 1

    # Calculate exponent u and num r to be used for test
    while r % 2 == 0:
        u += 1
        r //= 2

    # For loop to run a test s times
    for i in range(s):
        # Choose random number a in set (2 to num - 2)
        a = random.randrange(2, num - 2)
        # Perform (a^r mod num)
        z = sqrMult(a, r, num)
        # If result 'z' is not congruent to '1' or 'num - 1'
        if z != 1 and z != num - 1:
            # Then run this test (u - 1) times
            for j in range(u - 1):
                # Perform (z^2 mod num)
                z = sqrMult(z, 2, num)
                # If result 'z' is exactly 'num - 1' or not equal to '1'.
                if z == num - 1 or z != 1:
                    # Then exit for loop
                    break
            # Since it didn't pass in for loop after entering prior if statement
            # then the number is composite
            else:
                return print(str(num) + " is composite.")
    # If all other tests failed, then the number is most likely prime            
    return print(str(num) + " is likely prime.")


def main():

    # Problem 2
    p = 1090660992520643446103273789680343
    q = 1162435056374824133712043309728653
    d = 103
    y = 299604539773691895576847697095098784338054746292313044353582078965
    print('Plaintext is: ' + str(CRTDecrypt(y, p, q, d)))

    # Problem 3
    print('SquareMult of 2^79 mod 101 is: ' + str(sqrMult(2, 79, 101)))
    print('SquareMult of 3^197 mod 101 is: ' + str(sqrMult(3, 197, 101)))

   # Problem 5
    p = 31
    q = 37
    e = 17
    d = 953
    y = 2
    z = CRTDecrypt(y, p, q, d)
    print('Plaintext is: ' + str(z))
    print('RSA Encryption is: ' + str(RSAEncrypt(z, e, p, q)))

    # Problem 11
    s = 40

    pbar = 168909803230874138263387172095392667291729046012938418353288036816938314191260816867771276583217577384889182351734800484081273377039419410688677770265137334126561292914304220979495622574727698516240168400131857334861111178861992147966352890450829772502588074647763614313892185065266497807330814495282237159469
    millerRabin(pbar, s)

    pbar = 106670614970240670200388892920925406952427891862985189653223992120745249137014953455234070999351079636504210431386962383869627523012597459506154210219967205286540955027162953964946110615274774999065182431073519891886330334988925108448855780600651718961562210199310624775246274863276119789632794707966731523033
    millerRabin(pbar, s)

    pbar = 155531644568145728817795595855077119836519042079063126952651206271779851357904770211371296955888298255336867501262862572647273669672389989756116715181858961086957822712201248754302897858505507237345965549948796675504185591920315277298477201614001565975875473106445394896674559207964027030978919462622120478395
    millerRabin(pbar, s)

    pbar = 139799279929019408211773401894528733586532480325226174937947550330108919174271211946599441548661565211103816808100878622021105232604413401284277494065453748017072989440607968905719234750532728293035834196308691496149575946945806312527138321610691929456222340217295110611887333251366465017577245206536142245547
    millerRabin(pbar, s)

if __name__ == "__main__":
    main()