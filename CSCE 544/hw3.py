from importlib import import_module
import math
import binascii
import glob

def main():

    message = "Hope this Helps!"
    x = ''.join("{0:08b}".format(ord(c)) for c in message)
    print("Message in 8-bit ASCII is : " + x + '\n')

    # x = int(x, 2)
    # y = str(binascii.unhexlify('%x' % x))
    # print("Message in ASCII is : " + y + '\n')

    lfsr_output = "01001110001011110010100011000010000011111101010110011011101101001001110001011110010100011000010000011111101010110011011101101001"
    print("LFSR output is : " + lfsr_output + '\n')

    
    y = int(x, 2) ^ int(lfsr_output, 2)
    print("XOR output is : " + bin(y)[2:].zfill(len(x)))
 

if __name__ == '__main__':
    main()