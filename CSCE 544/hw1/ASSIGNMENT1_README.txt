# CSCE 544 - Data Security - HW Assignment #1
# Captain Kyle Hintze
# Code to encrypt/decrypt text using shift cipher
# Python Version # 3.8.0

In order to run code:

A. To encrypt or decrypt messages to test the function:
	1. First provide a key (i.e. any number such as 1, 2, 3, ...)
	2. Set the mode (i.e. encrypt or decrypt)
	3. Add a variable 'message' and pass text to encrypt, then pass the encrypted message
	   and set mode to decrypt to test that the function is working.
# Make sure to comment out 'fileContents_decrypt' and 'createFile' first to confirm that a simple
# test run can encrypt/decrypt messages.

B. To decrypt the HW1 assignment.
	1. Set key to '11'
	2. Set mode to 'decrypt'
	3. Run program and the decrypted '.tex' file will be produced
	4. Upload decrypted document in Latex editor of choice and compile to see the document.


# This program was run using Visual Studio code.
# In order to run in VS Code, at the bottom of the program, switch to the TERMINAL and invoke the 
# program using command, 'python Assignment1.py'.