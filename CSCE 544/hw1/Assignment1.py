# CSCE 544 - Data Security - HW Assignment #1
# Captain Kyle Hintze
# Code to encrypt/decrypt text using shift cipher
# Python Version # 3.8.0

from importlib import import_module
import glob

# Function to shift characters in a given message, based on the given key and mode
def shiftFunction(message,key,mode):
    SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz1234567890!?.=*\\{}%,^&$'
    translated = ''

    # If mode is set to 'encrypt', then this 'if' statement is entered
    if mode == 'encrypt':
        # For each letter in given message
        for letter in message:
            # If that letter is in the variable 'SYMBOLS'
            if letter in SYMBOLS:
                # Find first occurance of letter in 'SYMBOLS', add the key and mod
                # by the length of 'SYMBOLS'. Add to variable 'letter_index'.
                letter_index = (SYMBOLS.find(letter) + key) % len(SYMBOLS)
                # Add the character at index location from 'SYMBOLS' to variable 'translated'
                translated = translated + SYMBOLS[letter_index]
            # If not a letter
            else:
                # Add character to translated and assign to translated.
                translated = translated + letter

    # If mode is not set to 'encrypt', then default to decrypt.
    # Steps are the same as 'encrypt', except the key is subtracted in this mode.
    else:
        for letter in message:
            if letter in SYMBOLS:
                # Find first occurance of letter in 'SYMBOLS', subtract the key and mod
                # by the length of 'SYMBOLS'. Add to variable 'letter_index'.
                letter_index = (SYMBOLS.find(letter) - key) % len(SYMBOLS)
                translated = translated + SYMBOLS[letter_index]
            else:
                translated = translated + letter

    return translated

# From function to demo the readlines() function : https://www.techbeamers.com/python-read-file-line-by-line/
def readFile(fileToRead="HW1_Encrypted"):
    rd = open(fileToRead, "r")
    # Read list of lines
    out = []  # list to save lines
    while True:
        # Read next line
        line = rd.readline()
        # If line is blank, then you struck the EOF
        if not line:
            break;
        out.append(line.strip())
    # Close file
    rd.close()
    return out

# Function to create our test file, edited verion from :  https://www.techbeamers.com/python-read-file-line-by-line/
def createFile(ListOfLines=None, fileName="TestLatexFile.tex",key=98098098834,mode='decrypt'):
    wr = open(fileName, "w")
    for line in ListOfLines:
      # encrypt then save the line
      line = shiftFunction(line,key,mode)
      wr.write(line)
      wr.write("\n")
    wr.close()

# Read the file so we can decrypt it
key = 11
mode = 'decrypt'
fileContents_decrypt = readFile("HW1_Encrypted.tex")
createFile(fileContents_decrypt,"TestH1_Decrypted.tex",key,mode)  # produces a .tex output file