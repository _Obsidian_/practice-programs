from importlib import import_module
import binascii

# Function to calculate Round Key
def roundKey(key, round_i):

    # Create list called 'new_key' and initialize 'i'
    i = 1               
    new_Key = []

    # While the length of list 'key' has not been reached
    while (i != len(key)):
        new_Key.append(key[((i + round_i) - 2) % 9])   # Append to list 'new key' the subkey for round i
                                                       # starting with index i of original key     
        i += 1

    return new_Key

# DES Lite Encryption
def fTransformEncryption(L, R, round_i, key):

    # Expansion Box
    expansion_box = [1, 2, 4, 3, 4, 3, 5, 6]

    # S-Box's
    SBOX = [

    # S-Box 1
    [
    [5, 2, 1, 6, 3, 4, 7, 0],
    [1, 4, 6, 2, 0, 7, 5, 3]
    ],

    # S-Box 2
    [
    [4, 0, 6, 5, 7, 1, 3, 2],
    [5, 3, 0, 7, 6, 2, 1, 4]
    ]

    ]

    # Start Encryption
    # Begin Expansion
    new_R = []
    for i in expansion_box:
        new_R.append(R[i - 1])

    # Calculate Round Key
    key_i = roundKey(key, round_i)

    # XOR key_i w/ new_R
    xor_R = []
    for i in range(len(new_R)):
        xor_R.append(new_R[i] ^ key_i[i])

    # S-Box Row and Column Selection
    s_Row_1 = xor_R[0]
    s_Column_1 = xor_R[1:4]
    s_Row_2 = xor_R[4]
    s_Column_2 = xor_R[5:8]

    # Convert S-box Rows/Columns 1 to Dec and do Transform
    s_Row_Dec = int(str(s_Row_1), 2)
    s_Column_Dec = int("".join(str(i) for i in s_Column_1), 2)
    sbox_Transform_1 = SBOX[0][s_Row_Dec][s_Column_Dec]

    # Convert S-box Rows/Column 2 to Dec and do Transform
    s_Row_Dec = int(str(s_Row_2), 2)
    s_Column_Dec = int("".join(str(i) for i in s_Column_2), 2)
    sbox_Transform_2 = SBOX[1][s_Row_Dec][s_Column_Dec]

    # Concatenate S-Box values (Completion of "f" function)
    sbox_Concatenate = list(bin((sbox_Transform_1 << 3) + sbox_Transform_2)[2:].zfill(6))
    for i in range(len(sbox_Concatenate)):
        sbox_Concatenate[i] = int(sbox_Concatenate[i])

    # XOR with L_i
    R_i = []
    for i in range(len(sbox_Concatenate)):
        R_i.append(sbox_Concatenate[i] ^ L[i])

    # Assign Next round's L with Prior round's R
    L = R

    # Return new L and R values for next round
    return L, R_i

# DES Lite Decryption
def fTransformDecryption(L, R, round_i, key):

    # Expansion Box
    expansion_box = [1, 2, 4, 3, 4, 3, 5, 6]

    # S-Box's
    SBOX = [

    # S-Box 1
    [
    [5, 2, 1, 6, 3, 4, 7, 0],
    [1, 4, 6, 2, 0, 7, 5, 3]
    ],

    # S-Box 2
    [
    [4, 0, 6, 5, 7, 1, 3, 2],
    [5, 3, 0, 7, 6, 2, 1, 4]
    ]

    ]

    # Start Decryption
    # Begin Expansion
    new_L = []
    for i in expansion_box:
        new_L.append(L[i - 1])

    # Calculate Round Key
    key_i = roundKey(key, round_i)

    # XOR key_i w/ new_L
    xor_L = []
    for i in range(len(new_L)):
        xor_L.append(new_L[i] ^ key_i[i])

    # S-Box Row and Column Selection
    s_Row_1 = xor_L[0]
    s_Column_1 = xor_L[1:4]
    s_Row_2 = xor_L[4]
    s_Column_2 = xor_L[5:8]

    # Convert S-box Rows/Columns 1 to Dec and do Transform
    s_Row_Dec = int(str(s_Row_1), 2)
    s_Column_Dec = int("".join(str(i) for i in s_Column_1), 2)
    sbox_Transform_1 = SBOX[0][s_Row_Dec][s_Column_Dec]

    # Convert S-box Rows/Columns 2 to Dec and do Transform
    s_Row_Dec = int(str(s_Row_2), 2)
    s_Column_Dec = int("".join(str(i) for i in s_Column_2), 2)
    sbox_Transform_2 = SBOX[1][s_Row_Dec][s_Column_Dec]

    # Concatenate S-Box values (Completion of "f" function)
    sbox_Concatenate = list(bin((sbox_Transform_1 << 3) + sbox_Transform_2)[2:].zfill(6))
    for i in range(len(sbox_Concatenate)):
        sbox_Concatenate[i] = int(sbox_Concatenate[i])

    # XOR with R_i
    L_i = []
    for i in range(len(sbox_Concatenate)):
        L_i.append(sbox_Concatenate[i] ^ R[i])

    # Assign Next round's R with Prior round's L
    R = L

    # Return new R and L values for next round
    return R, L_i

# Function to Split Message into 6-bit Parts
def encryptionDES(newMessage, key, rounds):

    # Initialize "i" and starting round as 1 for encryption
    i = 0
    round_i = 1

    # Initialize empty list for each encrypted block of plaintext and the final encrypted message
    concatenatedMessage = []
    encryptedMessage = []

    # While the message to be encrypted hasn't been completely passed through cipher
    while (i < len(newMessage)):

        # Assign First 12-bits of message and split into 6-bit chunks
        L = newMessage[(i):(i + 6)]
        R = newMessage[(i + 6): (i + 12)]

        # While the # of rounds for each block has not been reached
        while (round_i < (rounds + 1)):

            # Call DES encryption on 12-bit input "L, R" and assign to tuple (L, R)
            (L, R) = fTransformEncryption(L, R, round_i, key)

            # Once a round is completed, increment to start next round
            round_i += 1

        # Reset round counter for next block of plaintext to be encrypted
        round_i = 1

        # Reassemble both 6-bits of encrypted text into one 12-bit message
        concatenatedMessage = L + R # orig:  R + L -- ok, fixed the flipped 6 bits problem here -- Lt Col King

        # Append 12-bit encrypted text to list
        encryptedMessage.append(concatenatedMessage)
        #print('concat_message:\t', concatenatedMessage) # testing purposes -- Lt Col King

        # Increment by 12 bits to send next block of text through encryption
        i = i + 12

    # Initialize empty list for ciphertext
    ciphertext = []        
    for i in encryptedMessage:                  # For each num in the encrypted message
        ciphertext.extend(i)                    # Extend the message and add to list "ciphertext"

    return ciphertext

# Function to Split Message into 6-bit Parts
def decryptionDES(encryptOutput, key, rounds):

    # Initialize "i" and starting round as "rounds" for decryption
    i = 0
    round_i = rounds

    # Initialize empty list for each decrypted block of plaintext and the final decrypted message
    concatenatedMessage = []
    decryptedMessage = []

    # While the message to be decrypted hasn't been completely passed through cipher
    while (i < len(encryptOutput)):

        # Assign First 12-bits of message and split into 6-bit chunks
        #R = encryptOutput[(i):(i + 6)]
        #L = encryptOutput[(i + 6):(i + 12)]

        #  According to the spec, you need to swap these around, otherwise you will decrypt wrong each time. -- Lt Col King
        L = encryptOutput[(i):(i + 6)]
        R = encryptOutput[(i + 6):(i + 12)]

        # While the # of rounds for each block has not been reached
        while (round_i > 0):

            # Call DES decryption on 12-bit input "R, L" and assign to tuple (R, L)
            (R, L) = fTransformDecryption(L, R, round_i, key)

            # Once a round is completed, decrment to start next round
            round_i -= 1
        
        # Reset round counter for next block of plaintext to be decrypted
        round_i = rounds

        # Reassemble both 6-bits of decrypted text into one 12-bit message
        concatenatedMessage = L + R

        # Append 12-bit decrypted text to list
        decryptedMessage.append(concatenatedMessage)

        # Increment by 12 bits to send next block of text through decryption
        i = i + 12

    # Initialize empty list for plaintext
    plaintext = []        
    for i in decryptedMessage:                  # For each num in the decrypted message
        plaintext.extend(i)                     # Extend the message and add to list "plaintext"

    return plaintext

# Function to convert text to 8-bit ASCII
def text2bin(message):

    binMessage = ''.join("{0:08b}".format(ord(c)) for c in message)     # Convert message to 8-bit binary
    list_binMessage = [int(i) for i in str(binMessage)]                 # Convert binary message to a list

    return list_binMessage

# Function to convert 8-bit ASCII to text
def bin2text(message):

    binMessage = int((''.join([str(i) for i in message])), 2)           # Convert to binary number
    binMessage = (binascii.unhexlify('%x' % binMessage))                # Convert to text
    binMessage = binMessage.decode("utf-8")                             # Decode and strip 'b'

    return binMessage

# Main
def main():

    # Begin code to list options, prompted to user
    while True:
        print('\nEnter Choice:')
        print('Option 1: Encrypt Message.')
        print('Option 2: Decrypt Message.')
        print('Option 3: Quit.\n')
        x = input()

        if (x == '1'):
            # Prompt user for message input
            print('\nEnter message to encrypt (Ensure that message, when encoded to 8-bit ASCII, is multiple of 12): ')
            message = input()
            for i in message:                                       #Newly added to account for unexpected character '\'
                message2 = message.rstrip().replace('\\', '')

            # Prompt user for key input
            key_input = input('\nEnter 9-bit key (each number seperated by a space): ')
            key_list = key_input.split()        # Remove spaces from user key
            key = [int(i) for i in key_list]    # Convert each character to an integer
            print(key)

            # Prompt user for number of Encrypt rounds
            print('\nEnter number of rounds: ')
            rounds = int(input())               # Convert input to an integer

            # Show message that will be encrypted
            print('\nPlaintext to be Encrypted: ' + message2)
            print("\nPlaintext in 8-bit ASCII:")
            newMessage = text2bin(message2)      # Convert message to 8-bit ASCII binary
            print(*newMessage)

            # Call Encryption function then print encrypted output
            encryptOutput = encryptionDES(newMessage, key, rounds)
            print("\nCiphertext after DES Lite encryption: ")
            print(*encryptOutput)

        elif (x == '2'):
            # Prompt user for message input
            print('\nEnter message to decrypt (Ensure message is 8-bit ASCII 1s and 0s w/ no spaces): ')
            message_input = input()
            message = [int(i) for i in str(message_input)]      # Convert each character to an integer

            # Prompt user for key input
            key_input = input('\nEnter 9-bit key (each number seperated by a space): ')
            key_list = key_input.split()        # Remove spaces from user key
            key = [int(i) for i in key_list]    # Convert each character to an integer
            print(key)

            # Prompt user for number of Encrypt rounds
            print('\nEnter number of rounds: ')
            rounds = int(input())               # Convert input to an integer

            # Show message that will be decrypted
            print('\nCiphertext to be Decrypted: ')
            print(*message)

            # Call Decryption function then print decrypted output
            decryptOutput = decryptionDES(message, key, rounds)
            print('\nPlaintext 8-bit ASCII binary after DES Lite decryption: ')
            print(*decryptOutput)

            # Call function to convert binary back to ASCII text
            newDecryptOutput = bin2text(decryptOutput)
            print('\nDecrypted ciphertext: ' + newDecryptOutput + '\n')

        else:
            # Exit program
            exit()

if __name__ == '__main__':
    main()
