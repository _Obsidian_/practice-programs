# CSCE 544 - Data Security - HW Assignment #2
# Captain Kyle Hintze
# Code to encrypt/decrypt text using affine cipher
# Python Version # 3.8.0

from importlib import import_module
import math
import glob

# Function to find the Bezout Coefficients using Euclid's Algorithm
def extEuclid(a,b):
    
    # Calls gcd() function on parameters 'a,b' using 'math'
    g = math.gcd(a, b)

    # If 'a' parameter is exactly '0'
    if a == 0:
        return (b, 0, 1)
    # If 'a' is anything other than '0'
    else:
        # Call extEculid recursively on 'a,b' and assign to variables 'g, x, y'
        g, x, y = extEuclid(b % a, a)
        # Return tuple (g, x, y)
        return (g, y - (b // a) * x, x)

# Fucntion to find all Multiplicative Inverses
def multiplicativeInverse(e_message, message_to_match, a, b, m):
    # Initialize 'i' to zero, create empty list of inverse's of 'a'
    i = 0 
    a_inverse = []

    # For each number in 'list a' call extEuclid() on that number and modulous 'm'
    # Assign to tuple (gcd, bezout_s, bezout_t). Append 'bezout_s' to list 'a_inverse'.
    for num1 in a:
        (gcd, bezout_s, bezout_t) = extEuclid(num1, m)
        a_inverse.append(bezout_s)

    # For each number in 'list a'
    for num1 in a:
        # For each number in 'list b', call shiftFunction() on provided message, key pair (num1, num2)
        # Current a_inverse index, and mode.  Assign to variable 'answer'.
        for num2 in b:
            answer = shiftFunction(e_message, num1, num2, a_inverse[i], 'decrypt')
            # If 'answer' matches 'message_to_match', key pair worked.  Print the successful key pair.
            if answer == message_to_match:
                print(num1, num2, a_inverse[i])
                break
            # If end of 'list b' has been reached, increment 'i' to use next a_inverse for next
            # a key in the loop iteration.
            else:
                if num2 == len(b):
                    i += 1


# Function to encrypt/decrypt messages using Affine Cipher
def shiftFunction(message,keyA,keyB,keyAInverse,mode):
    SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz1234567890!?.=*\\{}%,^&$'
    translated = ''

    # If mode is set to 'encrypt', then this 'if' statement is entered
    if mode == 'encrypt':
        # For each letter in given message
        for letter in message:
            # If that letter is in the variable 'SYMBOLS'
            if letter in SYMBOLS:
                # Find first occurance of letter in 'SYMBOLS', multiply by keyA, then add
                # keyB.  Mod by the length of 'SYMBOLS'.
                letter_index = ((keyA * SYMBOLS.find(letter)) + keyB) % len(SYMBOLS)
                # Add the character at index location from 'SYMBOLS' to variable 'translated'
                translated = translated + SYMBOLS[letter_index]
            # If not a letter    
            else:
                # Add character to translated and assign to translated.
                translated = translated + letter

    # If mode is not set to 'encrypt', then default to decrypt.
    # Steps are similar to 'encrypt'.
    else:
        for letter in message:
            if letter in SYMBOLS:
                # Find first occurance of letter in 'SYMBOLS', subtract keyB, then multiply
                # by keyAInverse. Mod by the length of 'SYMBOLS'.
                letter_index = (keyAInverse * (SYMBOLS.find(letter) - keyB)) % len(SYMBOLS)
                translated = translated + SYMBOLS[letter_index]
            else:
                translated = translated + letter

    return translated

# From function to demo the readlines() function : https://www.techbeamers.com/python-read-file-line-by-line/
def readFile(fileToRead="HW2_Template"):
    rd = open(fileToRead, "r")
    # Read list of lines
    out = []  # list to save lines
    while True:
        # Read next line
        line = rd.readline()
        # If line is blank, then you struck the EOF
        if not line:
            break
        out.append(line.strip())
    # Close file
    rd.close()
    return out

# Function to create our test file, edited verion from :  https://www.techbeamers.com/python-read-file-line-by-line/
def createFile(ListOfLines=None, fileName="TestLatexFile.tex",keyA=134,keyB=2334,keyInverseA = 342324,mode='encrypt'):
    wr = open(fileName, "w")
    for line in ListOfLines:
      # encrypt then save the line
      line = shiftFunction(line,keyA,keyB,keyInverseA,mode)
      wr.write(line)
      wr.write("\n")
    wr.close()

# Main Program 
def main(): 
    # Set encrypted message and decrypted message to match
    e_message = 'qSF=5?HU3?HQSF}AyZSUyO?FyFStyanO5,eHy,SU5yDS\\ntS5p '
    message_to_match = 'Congratulations! You can now decrypt your homework.'

    # Initialize 'list a' (keyA) and 'list b' (keyB).
    a = [1, 3, 5, 7, 9, 11, 13, 15, 17, 21, 23, 25, 27, 29, 31, 33, 35, 37, 
         39, 41, 43, 45, 47, 49, 51, 53, 55, 59, 61, 63, 65, 67, 69, 71, 73, 75]
    b = list(range(1, 75))

    # Begin code to list options, prompted to user
    while True:
        print('Enter Choice:')
        print('Option 1: See all possible keys for this assignment.')
        print('Option 2: Test Decryption on provided message for Assignment 2.')
        print('Option 3: Encrypt/Decrypt a message with your own key.')
        print('Option 4: Decrypt Assignment 2.')
        print('Option 5: Quit.\n')
        x = input()
        
        if (x == '1'):
            # List all legit keys (a,b) for assignment 2.
            print(multiplicativeInverse(e_message, message_to_match, a, b, 76))
            print('\n')

        elif (x == '2'):
            # Test any of the keys found in 'option 1' to decrypt message given for assignment 2.
            print('Provided message to decrypt is: ' + e_message)
            print('Enter in this order and fashion: "KeyA KeyB KeyAInverse mode"')
            keyA, keyB, keyAInverse, mode = [x for x in input().split()]
            answer = shiftFunction(e_message, int(keyA), int(keyB), int(keyAInverse), mode)
            print('Decrypted Message: ' + answer + '\n')

        elif (x == '3'):
            # Encrypt/decrypt arbitrary message with key pair that meets key requirements.
            print('Key requirements must be: GCD(KeyA, Modulus M) = 1 and ((KeyA*KeyAInverse)mod(M) congruent to 1')
            print('Enter your message.')
            message = input()
            print('Enter your key and mode in this fashion: "KeyA KeyB KeyAInverse mode"')
            keyA, keyB, keyAInverse, mode = [x for x in input().split()]
            answer = shiftFunction(message, int(keyA), int(keyB), int(keyAInverse), mode)
            print('Encrypted/Decrypted Message:' + answer + '\n')

        elif (x == '4'):
            # Decrypt assignment 2 using any key pair found in 'option 1'.
            print('Enter in this order and fashion: "KeyA KeyB KeyAInverse mode"')
            keyA, keyB, keyAInverse, mode = [x for x in input().split()]
            fileContents_decrypt = readFile('HW2_encrypted.tex')
            createFile(fileContents_decrypt, "HW2_Decrypted.tex", int(keyA), int(keyB), int(keyAInverse), mode)
            print('\n')

        else:
            # Exit program.
            exit()

if __name__ == "__main__":
    main()
