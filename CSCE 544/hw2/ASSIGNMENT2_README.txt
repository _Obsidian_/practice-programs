# CSCE 544 - Data Security - HW Assignment #2
# Captain Kyle Hintze
# Code to encrypt/decrypt text using affine cipher
# Python Version # 3.8.0

In order to run code:

A. Assignment2.py
Program has been set up to provide user with 5 different options.
	Option 1.  "See all possible keys for Assignment 2"
		-This option calculates all of the possible keys to decrypt Assignment 2.

	Option 2.  "Test Decryption on provide message for Assignment 2"
		-This option let's the user test one of the keys found in 'Option 1" on the 
		 provided message to see if the decryption works.

	Option 3.  "Encrypt/Decrypt a message with own key"
		-This let's the user encrypt/decrypt their own message with their own key,
		 provided they follow the key requirements.

	Option 4.  "Decrypt Assignment 2"
		-This will decrypt Assignment 2 with one of the key's found in 'Option 1'.

	Option 5. "Quit"
		-Quit Program


B.  HW_P2.py
	This is a modified version of the program made to decrypt Assignment 2.  The key
	found to decrypt the assignment is hardcoded.  All that needs to be done is to run
	the program and it will decrypt the message.  Decrypts to the oath of office.



# This program was run using Visual Studio code.
# In order to run in VS Code, at the bottom of the program, switch to the TERMINAL and invoke the 
# program using command, 'python Assignment2.py' or 'HW2_P2.py', depending on the program you want
# to run.